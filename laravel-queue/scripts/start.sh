#!/bin/sh

# RUN ARTISAN OPTIMIZE
php /var/www/html/artisan optimize

# PRINT START TIME
echo "Start Laravel Queue: $(date '+%Y-%m-%d %H:%M:%S')"

# RUN LARAVEL QUEUE
exec php /var/www/html/artisan queue:work
