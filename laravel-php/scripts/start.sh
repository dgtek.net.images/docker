#!/bin/sh

# CONFIGURE LARAVEL FOLDERS
if ls /var/www/html/storage/app/* &>/dev/null
then
    echo "Laravel folders exist in /var/www/html/storage"
else
    echo "Creating laravel folders in /var/www/html/storage"
    mkdir -p /var/www/html/storage/app/public
    mkdir -p /var/www/html/storage/framework/cache
    mkdir -p /var/www/html/storage/framework/cache/data
    mkdir -p /var/www/html/storage/framework/sessions
    mkdir -p /var/www/html/storage/framework/testing
    mkdir -p /var/www/html/storage/framework/views
    mkdir -p /var/www/html/storage/logs
fi

# GIVE PERMISSIONS TO STORAGE FOLDER
chmod -R 775 /var/www/html/storage

# RUN ARTISAN OPTIMIZE
php /var/www/html/artisan optimize

# PRINT START TIME
echo "Start PHP FPM: $(date '+%Y-%m-%d %H:%M:%S')"


# RUN PHP FPM SERVICE
exec php-fpm7 -R --force-stderr --nodaemonize
